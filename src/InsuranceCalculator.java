import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class with main method which gets the input from user and calculates the
 * premium amount to be paid
*/
enum Health {
	Hyepertension,
	Bloodpressure,
	Bloodsugar,
	Overweight
}

enum Habits {
	Smoking,
	Alcohol,
	DailyExercise,
	Drugs
}
public class InsuranceCalculator {
	public float rateCalculationBasedOnAge(int age) {
		float rate = 10f;
		if(age > 18 && age <= 25) {
			rate = (rate * 10) / 100;
		} else if (age > 25 && age <= 30) {
			rate += (rate * 20) / 100;
		} else if (age > 30 && age <= 35) {
			rate += (rate * 30) / 100;
		} else if (age > 35 && age <= 40) {
			rate += (rate * 40) / 100;
		} else if (age > 40 && age <= 45) {
			rate += (rate * 60) / 100;
		} else if (age > 45 && age <= 50) {
			rate += (rate * 80) / 100;
		} else if (age > 50 && age <= 55) {
			rate += (rate * 100) / 100;
		} else if (age > 55 && age <= 60) {
			rate += (rate * 120) / 100;
		} else if (age > 60 && age <= 65) {
			rate += (rate * 140) / 100;
		} else if (age > 65 && age <= 70) {
			rate += (rate * 160) / 100;
		} else if (age > 70 && age <= 75) {
			rate += (rate * 180) / 100;
		} else if (age > 75 && age <= 80) {
			rate += (rate * 200) / 100;
		}
		return rate;
	}
	public static void main(String args[]) {
		float basePremiumAmount = 5000f;
		Person person = new Person();
		Scanner userInput = new Scanner(System.in);
		System.out.print("Name : ");
		String name = userInput.nextLine();
		person.setName(name);
		System.out.print("Age : ");
		int age = userInput.nextInt();	
		person.setAge(age);
		System.out.print("Gender (Male/Female) : ");
		String gender = userInput.next();
		person.setGender(gender);
		
		List<Health> healthList = new ArrayList<Health>();
		for (Health h : Health.values()) {
			System.out.print("Do you have "+h+"?"+" (Y/N) : ");
			String answer = userInput.next();
			if(answer.equals("Y") || answer.equals("y") || answer.equals("yes")
					|| answer.equals("Yes")) {
				healthList.add(h);
			}
		}
		
		List<Habits> goodHabits = new ArrayList<Habits>();
		List<Habits> badHabits = new ArrayList<Habits>();
		for(Habits habits : Habits.values()) {
			System.out.println("Do you have the habit of "+habits+"?"+" (Y/N) : ");
			String habitAnswer = userInput.next();
			Habits habitCheck = habits;
			if(habitAnswer.equals("Y") || habitAnswer.equals("y") 
					|| habitAnswer.equals("yes")
					|| habitAnswer.equals("Yes")) {
				if(habitCheck == habits.Alcohol
					|| habitCheck == habits.Drugs
					|| habitCheck == habits.Smoking) {
					badHabits.add(habits);
				} else {
					goodHabits.add(habits);
				}
			}
		}
		
		InsuranceCalculator ic = new InsuranceCalculator();
		float rateCalculator = ic.rateCalculationBasedOnAge(person.getAge());
		rateCalculator += healthList.size();
		rateCalculator += badHabits.size() * 3;
		rateCalculator -= goodHabits.size() * 3;
		if(person.getGender().compareTo("M") == 0 
			|| person.getGender().compareTo("m") == 0
			|| person.getGender().compareTo("Male") == 0
			|| person.getGender().compareTo("male") == 0) {
			rateCalculator += 2;
		}
		basePremiumAmount = basePremiumAmount+(basePremiumAmount*rateCalculator)/100;
		System.out.println("Health insurance premium for "+person.getName()+":"+basePremiumAmount);
	}
}